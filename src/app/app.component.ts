import {Component} from '@angular/core';
import {Ingredient} from "./shared/ingridient.model";
import {parse} from "@angular/compiler/src/render3/view/style_parser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  listOfIng: Ingredient[] = [
    new Ingredient('Meat', 0, 50, 'http://s3.amazonaws.com/kidzworld_photo/images/201351/2af2a979-3994-447b-8cd4-b19d06f42337/gallery_hamburger-gallery2.jpg'),
    new Ingredient('Cheese', 0, 20, 'https://cheeseman1.com/wp-content/uploads/2015/10/cheedar-cheese-300x300.png'),
    new Ingredient('Salad', 0, 5, 'http://bk-ca-prd.s3.amazonaws.com/sites/burgerking.ca/files/THMB-Garden_Side_Salad_0.png'),
    new Ingredient('Bacon', 0, 30, 'https://tienda.albadanatural.es/8601-large_default/bacon-lonchas-100gr-biobardales.jpg')
  ];


  addAmount(index: number) {
    this.listOfIng[index].amount++;
  }

  reduceAmount(index: number) {
    this.listOfIng[index].amount--;
  }

}
