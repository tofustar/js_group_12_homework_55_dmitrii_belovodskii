import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-burger',
  templateUrl: './burger.component.html',
  styleUrls: ['./burger.component.css']
})
export class BurgerComponent {
  @Input() salad = '';
  @Input() cheese = '';
  @Input() meat = '';
  @Input() bacon = '';
}
