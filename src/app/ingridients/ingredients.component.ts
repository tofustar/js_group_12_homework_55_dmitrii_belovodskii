import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent {
  @Input() name = '';
  @Input() src = '';
  @Input() amount = 0;
  @Output() addAmount = new EventEmitter();
  @Output() reduceAmount = new EventEmitter();

  addIng() {
    this.addAmount.emit();
  }

  deleteIng() {
    this.reduceAmount.emit();
  }
}


