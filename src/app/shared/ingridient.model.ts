export class Ingredient {
constructor(
  public name:string,
  public amount:number,
  public price:number,
  public image: string
) {}

  getPrice(){
    return this.price * this.amount;
  }
}

